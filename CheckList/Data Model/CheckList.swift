//
//  CheckListArray.swift
//  CheckList
//
//  Created by Hui Chih Wang on 2020/12/3.
//

import Foundation

class CheckList: Codable {
    var name: String
    var iconName: String
    var items: [CheckItem]
    
    var uncheckedItemsCount: Int {
        var count = 0
        
        items.forEach { item in
            if !item.isDone {
                count += 1
            }
        }
        
        return count
    }
    
    var isEmpty: Bool {
        items.isEmpty
    }
    
    init(name: String = "default", iconName: String = "No Icon", items: [CheckItem] = [CheckItem]()) {
        self.name = name
        self.iconName = iconName
        self.items = items
    }
    
    
    public func insertItem(for item: CheckItem) {
        items.append(item)
    }
    
    public func removeItem(for item: CheckItem) {
        if let itemFoundIndex = items.foundItemIndex(for: item) {
            items.remove(at: itemFoundIndex)
        }
    }
    
    public func chooseItem(for item: CheckItem) {
        if let itemFoundIndex = items.foundItemIndex(for: item) {
            items[itemFoundIndex].isDone.toggle()
        }
    }
    
    public func editItem(for item: CheckItem) {
        if let itemFoundIndex = items.foundItemIndex(for: item) {
            items[itemFoundIndex] = item
        }
    }
    
    public func indexItem(for item: CheckItem) -> Int? {
        items.foundItemIndex(for: item)
    }
}

fileprivate extension Array where Element:Identifiable {
    func foundItemIndex(for item: Element) -> Int? {
        self.firstIndex(where: { $0.id == item.id})
    }
}
