//
//  CheckLists.swift
//  CheckList
//
//  Created by Hui Chih Wang on 2020/12/8.
//

import Foundation

class CheckLists {
    var lists = [CheckList]()
    var numberOfLists: Int {
        lists.count
    }
    
    var nameLists: [String] {
        lists.map { $0.name }
    }
    
    public func addNewList(_ checklist: CheckList) {
        lists.append(checklist)
    }
    
    public func removeList(at index: Int) {
        lists.remove(at: index)
    }
    
    public func indexList(_ checklist: CheckList) -> Int? {
        lists.firstIndex { $0.name == checklist.name }
    }
    
    public func sortLists() {
        lists.sort { list1, list2 in
            list1.name < list2.name
        }
    }
}


