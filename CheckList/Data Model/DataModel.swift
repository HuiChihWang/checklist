//
//  DataModel.swift
//  CheckList
//
//  Created by Hui Chih Wang on 2020/12/10.
//

import Foundation

class DataModel {
    let keyForDefaultList = "ChecklistIndex"
    
    var checkLists = CheckLists()
        
    var documentsDirectory : URL {
        let paths = FileManager.default.urls(
            for: .documentDirectory,
            in: .userDomainMask)
        return paths[0]
    }
    
    var dataFilePath: URL {
        return documentsDirectory.appendingPathComponent(keyForDefaultList)
    }
    
    var indexOfSelectedChecklist: Int {
      get {
         UserDefaults.standard.integer(forKey: keyForDefaultList)
      }
      set {
        UserDefaults.standard.set(newValue, forKey: keyForDefaultList)
      }
    }
    
    private var isCheckListExist: Bool {
        checkLists.lists.indices.contains(indexOfSelectedChecklist)
    }
    
    var previousCheckList: CheckList? {
        isCheckListExist ? checkLists.lists[indexOfSelectedChecklist] : nil
    }
    
    init() {
        print("Path: \(self.documentsDirectory)")
        loadChecklists()
        registerDefaults()
        handleFirstTime()
    }
    
    func registerDefaults() {
        let dictionary: [String:Any] = [
        "ChecklistIndex": -1,
        "FirstTime": true
        ]
        
      UserDefaults.standard.register(defaults: dictionary)
    }

    func handleFirstTime() {
      let userDefaults = UserDefaults.standard
      let firstTime = userDefaults.bool(forKey: "FirstTime")

      if firstTime {
        let checklist = CheckList(name: "List")
        checkLists.lists.append(checklist)

        indexOfSelectedChecklist = 0
        userDefaults.set(false, forKey: "FirstTime")
      }
    }

    
    // MARK: - save/load data function
    func saveChecklists() {
        let encoder = PropertyListEncoder()
        
        do {
            let data = try encoder.encode(checkLists.lists)
            try data.write(
                to: dataFilePath,
                options: Data.WritingOptions.atomic)
        } catch {
            print("Error eecoding list array: \(error.localizedDescription)")
        }
    }
    
    func loadChecklists() {
        
        if let data = try? Data(contentsOf: dataFilePath) {
            
            let decoder = PropertyListDecoder()
            
            do {
                checkLists.lists = try decoder.decode([CheckList].self, from: data)
                checkLists.sortLists()
            } catch {
                print("Error decoding list array: \(error.localizedDescription)")
            }
        }
    }
}
