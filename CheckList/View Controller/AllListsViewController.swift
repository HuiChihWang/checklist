//
//  AllListsViewController.swift
//  CheckList
//
//  Created by Hui Chih Wang on 2020/12/8.
//

import UIKit

class AllListsViewController: UITableViewController, ListDetailViewControllerDelegate, UINavigationControllerDelegate {
    
    let cellIdentifier = "ChecklistCell"
    var checkListsModel: DataModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        navigationController?.delegate = self
        
        if let previousCheckList = checkListsModel.previousCheckList {
            performSegue(withIdentifier: "ShowChecklist", sender: previousCheckList)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    
    
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowChecklist" {
            let controller = segue.destination as! ChecklistViewController
            controller.checklist = sender as? CheckList
        }
        else if segue.identifier == "AddChecklist" {
            let controller = segue.destination as! ListDetailViewController
            controller.delegate = self
        }
    }
    
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checkListsModel.checkLists.numberOfLists
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell!
        
        if let tmp = tableView.dequeueReusableCell(withIdentifier: cellIdentifier){
            cell = tmp
        }
        else {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
        
        let checkListAtRow = checkListsModel.checkLists.lists[indexPath.row]
        let itemCount = checkListAtRow.uncheckedItemsCount
        
        
        cell.detailTextLabel!.text = itemCount == 0 ? "All Done" : "\(itemCount) Remaining"
        if checkListAtRow.isEmpty {
            cell.detailTextLabel!.text = "(No Items)"
        }
        cell.textLabel!.text = checkListAtRow.name
        cell.imageView!.image = UIImage(named: checkListAtRow.iconName)
        cell.accessoryType = .detailDisclosureButton
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath
    ) {
        checkListsModel.indexOfSelectedChecklist = indexPath.row
        
        let checklist = checkListsModel.checkLists.lists[indexPath.row]
        performSegue(withIdentifier: "ShowChecklist", sender: checklist)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath){
        checkListsModel.checkLists.removeList(at: indexPath.row)
        
        let indexPaths = [indexPath]
        tableView.deleteRows(at: indexPaths, with: .automatic)
    }
    
    
    // perform segue to edit view
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath){
        let controller = storyboard!.instantiateViewController(
            withIdentifier: "ListDetailViewController") as! ListDetailViewController
        controller.delegate = self
        
        let checklist = checkListsModel.checkLists.lists[indexPath.row]
        controller.checklistToEdit = checklist
        
        navigationController?.pushViewController(
            controller,
            animated: true)
    }
    
    // MARK: - Navigation Controller Delegates
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        // Was the back button tapped?
        if viewController === self {
            checkListsModel.indexOfSelectedChecklist = -1
        }
    }
    
    // MARK: - List Detail View Controller Delegates
    func listDetailViewControllerDidCancel(_ controller: ListDetailViewController){
        navigationController?.popViewController(animated: true)
    }
    
    func listDetailViewController(_ controller: ListDetailViewController, didFinishAdding checklist: CheckList){
        checkListsModel.checkLists.lists.append(checklist)
        checkListsModel.checkLists.sortLists()
        tableView.reloadData()
        navigationController?.popViewController(animated: true)
    }
    
    func listDetailViewController(_ controller: ListDetailViewController, didFinishEditing checklist: CheckList){
        checkListsModel.checkLists.sortLists()
        tableView.reloadData()
        navigationController?.popViewController(animated: true)
    }
    
}
