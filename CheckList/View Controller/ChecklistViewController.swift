//
//  ViewController.swift
//  CheckList
//
//  Created by Hui Chih Wang on 2020/12/3.
//

import UIKit

class ChecklistViewController: UITableViewController, ItemDetailViewControllerDelegate {
    var checklist: CheckList!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = checklist.name
    }
    
    // MARK: - Navigation
    override func prepare(
        for segue: UIStoryboardSegue,
        sender: Any?
    ) {
        
        switch segue.identifier {
        case "AddItem":
            let controller = segue.destination as! ItemDetailViewController
            controller.delegateFromItemView = self
        case "EditItem":
            let controller = segue.destination as! ItemDetailViewController
            controller.delegateFromItemView = self
            
            if let indexPath = tableView.indexPath( for: sender as! UITableViewCell) {
                controller.itemToEdit = checklist.items[indexPath.row]
            }
        default:
            break
        }
    }
    
    
    // MARK: - Table View Data Source
    override func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        return checklist.items.count
    }
    
    override func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "ChecklistItem",
            for: indexPath)
        
        let item = checklist.items[indexPath.row]
        configureText(for: cell, with: item)
        configureCheckmark(for: cell, with: item)
        
        return cell
    }
    
    override func tableView(
        _ tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    ) {
        if let cell = tableView.cellForRow(at: indexPath) {
            let item = checklist.items[indexPath.row]
            checklist.chooseItem(for: item)
            configureCheckmark(for: cell, with: item)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(
        _ tableView: UITableView,
        commit editingStyle: UITableViewCell.EditingStyle,
        forRowAt indexPath: IndexPath
    ) {
        // 1
        let removeItem = checklist.items[indexPath.row]
        checklist.removeItem(for: removeItem)
        
        // 2
        let indexPaths = [indexPath]
        tableView.deleteRows(at: indexPaths, with: .automatic)
    }
    
    
    // MARK: - Add item delegate
    func itemDetailViewControllerDidCancel(_ controller: ItemDetailViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    func itemDetailViewController(
        _ controller: ItemDetailViewController,
        didFinishAdding item: CheckItem
    ) {
        checklist.insertItem(for: item)
        addNewItemInView()
        navigationController?.popViewController(animated:true)
    }
    
    func itemDetailViewController(_ controller: ItemDetailViewController, didFinishEditing item: CheckItem) {
        if let index = checklist.indexItem(for: item) {
            let indexPath = IndexPath(row: index, section: 0)
            if let cell = tableView.cellForRow(at: indexPath) {
                checklist.editItem(for: item)
                configureText(for: cell, with: item)
                navigationController?.popViewController(animated: true)
            }
        }
    }
    
    
    // MARK: - Helper function
    private func configureCheckmark(
        for cell: UITableViewCell,
        with item: CheckItem
    ) {
        let label = cell.viewWithTag(1001) as! UILabel
        
        if item.isDone {
            label.text = "√"
        } else {
            label.text = ""
        }
    }
    
    private func configureText(
        for cell: UITableViewCell,
        with item: CheckItem) {
        let label = cell.viewWithTag(1000) as! UILabel
        label.text = item.text
    }
    
    private func addNewItemInView() {
        let lastRowIndex = tableView.numberOfRows(inSection: 0)
        tableView.insertRows(at: [IndexPath(row: lastRowIndex, section: 0)], with: .automatic)
    }
    
    
    
}
