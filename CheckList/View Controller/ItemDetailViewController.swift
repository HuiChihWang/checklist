//
//  AddItemTableViewController.swift
//  CheckList
//
//  Created by Hui Chih Wang on 2020/12/4.
//

import UIKit
import UserNotifications

protocol ItemDetailViewControllerDelegate: AnyObject {
    func itemDetailViewControllerDidCancel(_ controller: ItemDetailViewController)
    func itemDetailViewController(_ controller: ItemDetailViewController, didFinishAdding item: CheckItem)
    func itemDetailViewController(_ controller: ItemDetailViewController, didFinishEditing item: CheckItem)
}


class ItemDetailViewController: UITableViewController, UITextFieldDelegate {
    
    var itemToEdit: CheckItem?
    weak var delegateFromItemView: ItemDetailViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let item = itemToEdit {
            self.title = "Edit Item"
            taskInput.text = item.text
            doneBarButton.isEnabled = true
            shouldRemindSwitch.isOn = item.shouldRemind
            datePicker.date = item.dueDate
        }
        
    }
    
    //MARK: - Data View
    @IBOutlet weak var taskInput: UITextField!
    @IBOutlet weak var doneBarButton: UIBarButtonItem!
    @IBOutlet weak var shouldRemindSwitch: UISwitch!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    // MARK: - Actions
    @IBAction func shouldRemindToggled(_ switchControl: UISwitch) {
        taskInput.resignFirstResponder()
        
        if switchControl.isOn {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound]) {_, _ in
                // do nothing
            }
        }
    }
    
    @IBAction func cancel() {
        delegateFromItemView?.itemDetailViewControllerDidCancel(self)
    }
    
    @IBAction func done() {
        if let itemToEdit = self.itemToEdit {
            itemToEdit.text = taskInput.text!
            itemToEdit.shouldRemind = shouldRemindSwitch.isOn
            itemToEdit.dueDate = datePicker.date
            itemToEdit.scheduleNotification()
            delegateFromItemView?.itemDetailViewController(self, didFinishEditing: itemToEdit)
        }
        else {
            let item = CheckItem(text: taskInput.text!)
            item.shouldRemind = shouldRemindSwitch.isOn
            item.dueDate = datePicker.date
            item.scheduleNotification()
            delegateFromItemView?.itemDetailViewController(self, didFinishAdding: item)
        }
    }
    
    // disable gray when select on cell
    override func tableView(
        _ tableView: UITableView,
        willSelectRowAt indexPath: IndexPath
    ) -> IndexPath? {
        return nil
    }
    
    // enable typing test as soon as entering add view
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        taskInput.becomeFirstResponder()
    }
    
    // MARK: - Text Field Delegates -> UITextFieldDelegate
    func textField(
        _ textField: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool {
        let oldText = textField.text!
        let stringRange = Range(range, in: oldText)!
        let newText = oldText.replacingCharacters(
            in: stringRange,
            with: string)
        
        doneBarButton.isEnabled = !newText.isEmpty
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
      doneBarButton.isEnabled = false
      return true
    }

}
